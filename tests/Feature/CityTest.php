<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Migrations\Migration;
use App\City;


class CityTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * A test function to find city name by ID.
     *
     * @return void
     */
    public function testFindCityByName()
    {
    	$find_city = City::find(608);
      	$this->assertEquals($find_city->name, "Hurzuf");
       	// $this->assertTrue(true);
    }

    /**
     * A test function to insert a city .
     *
     * @return void
     */
    public function testInsertCity()
    {
    	$city 			= new City;
        $city->name 	= "Test City Name";
        $city->country 	= "Test";
        $city->save();

        $this->seeInDatabase('city_list',['name'=> "Test City Name" ,'country'=> "Test"]);
        //$this->assertTrue(true);
    }

    /**
     * A test function for delete a city .
     *
     * @return void
     */
    public function testDeleteCity()
    {

    	$find_city = City::find(210199);
        $this->assertTrue($find_city->delete());
    
    }

    
}
