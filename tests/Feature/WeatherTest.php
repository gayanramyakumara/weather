<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WeatherTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    
    /**
     * A basic functional test example.
     *
     * @return void
     */

    public function testGetWether()
    {
        $this->withoutExceptionHandling();

        try {

         $response =    $this->post('/', [
            'city_name' => 'Norway'
           ]);

         $this->assertEquals('OK', $response->getContent());


        } catch (HttpException $e) {
            $this->assertEquals(Response::HTTP_FORBIDDEN, $e->getStatusCode());
            $this->assertEquals('The webhook signature was invalid.', $e->getMessage());
            return;
        }

        //$this->fail('Expected the webhook signature to be invalid.');
    }

}
