<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('weather/history', 'WeatherController@history');
// Route::get('weather/cityList', 'WeatherController@cityList')->name('city_list');
Route::get('weather/current', 'WeatherController@current');
Route::get('weather/history', 'WeatherController@history');
// Route::get('weather/home', 'WeatherController@home');
Route::get('/', 'WeatherController@home');
Route::post('/', 'WeatherController@home');

Route::get('weather/addCity', 'WeatherController@addCity');
Route::get('city/find_city', 'CityController@findByCityName');

// Route::get('/', function () {
//     return view('welcome');
// });
