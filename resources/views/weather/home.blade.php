<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

		<title>Compass Starter by Ariona, Rian</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<!-- <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
		<link href="{{ asset('custom/fonts/font-awesome.min.css') }}" rel="stylesheet">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="{{ asset('custom/style.css') }}">

		 <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" /> 


		<!-- jQuery CDN -->
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

		<script type="text/javascript">
			$(document).ready(function(){

				//autocomplete
				$(".auto").autocomplete({
					source: "{{ url('city/find_city')}}",
					minLength: 1
				});

			});

		</script>


	</head>


	<body>

		<div class="site-content">

			<div class="hero" data-bg-image="{{ asset('custom/images/banner.png') }}">
				<div class="container">
					<form action="{{url('/')}}" method="POST" class="find-location">
						<input type="text" id="basics" name="city_name" class="auto" placeholder="Find your location...">
						<input type="submit" id="find_btn" value="Find">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>

				</div>
			</div>

			<div class="forecast-table">
				<div class="container">
									
					<div class="forecast-container">
						@if( $get_details_history != false)
						<div class="today forecast">
							<div class="forecast-header">
								<div class="day">{{$weather_data['current_day_name']}}</div>
								<div class="date">{{$weather_data['current_month_date']}}</div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content">
								 
								<div class="location"> <p  id="location_name"> {{$weather_data['city_name']}} </p></div>
								<div class="degree">
									<div class="num"><p id="temperature_now"> {{ $weather_data['temperature_now'] }} </p> </div>
									<div class="forecast-icon">
										<!-- <img src="{{ asset('custom/images/icons/icon-1.svg') }}" alt="" width=90> -->
										<img src="http://openweathermap.org/img/w/{{ $weather_data['current_weather_icon'] }}" alt="" width=90>
									</div>
								</div>
								<span><img src="{{ asset('custom/images/icon-umberella.png') }}" alt="">{{ $weather_data['humidity'] }}</span>
								<span><img src="{{ asset('custom/images/icon-wind.png') }}" alt="">{{ $weather_data['current_wind_speed'] }}</span>
								<span><img src="{{ asset('custom/images/icon-compass.png') }}" alt="">{{ $weather_data['current_wind_direction'] }}</span>
							</div>
						</div>

						@foreach($get_details_history->original as $get_history)

						 
						<div class="forecast">
							<div class="forecast-header">
								<div class="day">{{ $get_history['day_name'] }}</div>
							</div> <!-- .forecast-header -->
							<div class="forecast-content">
								<div class="forecast-icon">
									<!-- <img src="{{ asset('custom/images/icons/icon-3.svg') }}" alt="" width=48> -->
									<img src="http://openweathermap.org/img/w/{{$get_history['weather_icon']}}" alt="" width=48>
								</div>
								<div class="degree">{{ $get_history['temperature'] }}</div>
								<p>{{ $get_history['weather_des']}}</p>
								<!-- <small>18<sup>o</sup></small> -->
								<small>from {{ $get_history['from'] }} to {{ $get_history['to'] }} </small>
							</div>
						</div>
						@endforeach

					@else
						<h2>No Result Found....<h2>
					@endif
 
					</div>


				</div>
			</div>

			
			<footer class="site-footer">
				<div class="container">
					<div class="row">						
					<p class="colophon">Gapsters Technical Assigment</p>
				</div>
			</footer> <!-- .site-footer -->
		</div>

		<!-- <script src="{{ asset('custom/js/jquery-1.11.1.min.js') }}"></script> -->
		<script src="{{ asset('custom/js/plugins.js') }}"></script>
		<script src="{{ asset('custom/js/app.js') }}"></script>

	</body>

</html>
