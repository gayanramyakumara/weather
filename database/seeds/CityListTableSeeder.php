<?php

use Illuminate\Database\Seeder;

class CityListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('city_list')->delete();
        $json = File::get("storage/app/city_list.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          User::create(array(
            'id' => $obj->id,
            'name' => $obj->name,
            'country' => $obj->country
          ));
        }


    }
}
