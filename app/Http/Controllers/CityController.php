<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;
use Illuminate\Support\Facades\Input;

/**
 * Created by PhpStorm.
 * Date: 2017-Dec-11
 * Time: 11:02 AM
 * Gapsters Assigments
 * CityController Helper
 *
 * @author       Gayan Ramya Kumara
 * @version      $Id: v1.2.0 Exp $;
 */

class CityController extends Controller
{
   

       /**
     *
     * Function name : findByCityName
     * Ths function will rerivew all the specified resource from the mysql storage.
     * @param  string  $city_name
     * @return array ($return_arr)
     */
    public function findByCityName()
    {
        try {
            $return_arr = array();
            
            $city_name = Input::get('term');
            $result    = City::where('name', '=', $city_name)->select('name')->get();
            
            foreach ($result as $value) {
                $return_arr[] = $value['name'];
            }
            
        }
        catch (Exception $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        
        /* back results as json encoded array. */
        echo json_encode($return_arr);
        
    }


}
