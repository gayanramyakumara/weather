<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gmopx\LaravelOWM\LaravelOWM;
use Zjango\Curl\Facades\Curl;
use Storage;
use Illuminate\Support\Facades\Input;

/**
 * Created by PhpStorm.
 * Date: 2017-Dec-11
 * Time: 07:23 AM
 * Gapsters Assigments
 * WeatherController Helper
 *
 * @author       Gayan Ramya Kumara
 * @version      $Id: v1.2.0 Exp $;
 */

class WeatherController extends Controller
{
    
    
    /**
     * This function will review current weather details and history.
     *
     * @param string $city_name 
     * @return array ('weather_data','get_details_history','get_details_city')
     */
    
    public function home(Request $request)
    {
        
        $city_name = ($request->input('city_name') != "") ? $request->input('city_name') : "colombo";
        
        $get_details_history = $this->history($city_name);
        
        if ($this->history($city_name) != FALSE) {
            $get_details_history = $this->history($city_name);
        } else {
            $get_details_history = false;
        }
        
        $get_details_current = $this->current($city_name);
        //$get_details_city     = $this->cityList();

        $weather_data = array(
            'city_name' 		=> $get_details_current->city->name,
            'temperature_now' 	=> $get_details_current->temperature->now,
            'temperature_min' 	=> $get_details_current->temperature->min,
            'temperature_max' 	=> $get_details_current->temperature->max,
            'humidity' 			=> $get_details_current->humidity,
            'weather' 			=> $get_details_current->weather,
            'current_day_name' 	=> $get_details_current->lastUpdate->format('l'),
            'current_month_date'=> $get_details_current->lastUpdate->format('F d'),
            'current_wind_speed'=> $get_details_current->wind->speed,
            'current_wind_direction'=> $get_details_current->wind->direction,
            'current_weather_icon'=> $get_details_current->weather->icon.".png"
        );
        
        return view('weather.home', compact('weather_data', 'get_details_history', 'get_details_city'));
        
    }
    
     /**
     * This function will review current weather details.
     *
     * @param string $city_name 
     * @return array ('weather_data')
     */

    public function current($city_name)
    {
        $lowm            = new LaravelOWM();
        $current_weather = $lowm->getCurrentWeather($city_name);
        return $current_weather;
        //dd($current_weather);
        
    }

     /**
     * This function will review five days weather history.
     *
     * @param string $city_name 
     * @return array ('get_details_history')
     */
    public function history($city_name)
    {
        $lowm = new LaravelOWM();
        $arr  = array();
        
        try {
            
            $get_details_history = $lowm->getDailyWeatherForecast($city_name);
            
            foreach ($get_details_history as $weather) {
                
                $weather_history = array();
                
                $weather_history['day']         = $weather->time->day->format('Y-m-d');
                $weather_history['day_name']    = $weather->time->day->format('l');
                $weather_history['month_date']  = $weather->time->day->format('F d');
                $weather_history['from']        = $weather->time->from->format('H:i');
                $weather_history['to']          = $weather->time->to->format('H:i');
                $weather_history['temperature'] = $weather->temperature;
                $weather_history['sun_rise']    = $weather->sun->rise->format('Y-m-d H:i (e)');
                $weather_history['weather_icon']    = $weather->weather->icon.".png";             
            	$weather_history['weather_des'] = $weather->weather->description;
                
                array_push($arr, $weather_history);
            }
            
            return response($arr);
            
        }
        catch (\Exception $e) {
            return false; //$e->getMessage();
        }
        
    }
    
    
}